# ZeroFuruit

Next-gen ZeroFuruit for a fruit game.
> This codebase contains the reworked script, if you wish to see the previous script, please see [GitHub](https://github.com/teppyboy/RbxScripts/blob/master/Misc/ZeroFuruit/ZeroFuruit.lua)

## Description

ZeroFuruit is a script with various features for a fruit game in ROBLOX. We aim to make the script as simple as possible, while also having as much features as we can.

## Compatibility

### Compatible

+ Synapse X
  + This script is coded with the help from Synapse X Documentation, many thanks to them for their amazing documentation which others don't have.

+ Krnl
  + Being the best free exploit (and better than some paid ones), Krnl should fully supports this script.

+ SirHurt
  + I hate SirHurt because of its owner but it's the only one I have that works in Wine.

### Not compatible

+ JJSploit and WeAreDevs' based exploit
  + Too much memory leaks, unstable performance, unimplemented function.

## Badges

TODO

## Usage

```lua
loadstring(game:HttpGet("<not available atm>"))
```

## Support

Open an issue in this repository if you found any problem

## Roadmap

TODO

## Contributing

TODO

## Authors and acknowledgment

+ Krnl, SirHurt for exploit and Synapse X for documentation, otherwise this script couldn't be made

+ [librequire](https://gitlab.com/ZeroScripts/librequire) for simple require

+ Probably more...

## License

See [LICENSE](./LICENSE)

## Project status

If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
